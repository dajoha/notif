use clap::{Arg, App, crate_version};
use anyhow::Result;

/// Stores the configuration retrieved from the command-line arguments.
#[derive(Debug)]
pub struct CliConfig {
    pub title: Option<String>,
    pub body: Option<String>,
    pub group: Option<String>,
    pub timeout: Option<i32>,
    pub close_group: Option<String>,
}

impl CliConfig {
    pub fn new() -> Result<CliConfig> {
        let matches = Self::cli_definition().get_matches();

        let timeout: Option<i32> = match matches.value_of("timeout") {
            Some(s) => Some(s.parse::<i32>()?),
            _ => None,
        };

        let title = matches.value_of("title")
            .or_else(|| matches.value_of("title-as-arg"))
            .map(|v| v.to_owned());
        let body = matches.value_of("body").map(|v| v.to_owned());
        let close_group = matches.value_of("close-group").map(|v| v.to_owned());

        Ok(Self {
            title,
            body,
            group: matches.value_of("group").map(|v| v.to_owned()),
            timeout,
            close_group,
        })
    }

    /// Returns the command-line argument definition for clap.
    fn cli_definition<'a, 'b>() -> App<'a, 'b> {
        App::new("notif")
            .about("Sends a GUI notification via dbus.")
            .version(crate_version!())
            .arg(Arg::with_name("title-as-arg")
                .takes_value(true)
                .value_name("TITLE")
                .help("Title of the notification"))
            .arg(Arg::with_name("title")
                .short("t")
                .long("title")
                .takes_value(true)
                .value_name("TITLE")
                .help("Title of the notification (can be given as a basic argument instead)"))
            .arg(Arg::with_name("body")
                .short("b")
                .long("body")
                .takes_value(true)
                .value_name("BODY")
                .help("Body of the notification"))
            .arg(Arg::with_name("group")
                .short("g")
                .long("group")
                .takes_value(true)
                .value_name("GROUP")
                .help("Assign the notification to the given group name (useful when needing to replace/close the notification)"))
            .arg(Arg::with_name("close-group")
                .short("c")
                .long("close")
                .takes_value(true)
                .value_name("GROUP")
                .help("Close the given group (ignore all other options)"))
            .arg(Arg::with_name("timeout")
                .short("T")
                .long("timeout")
                .takes_value(true)
                .value_name("TIMEOUT")
                .help("Timeout in ms before the notification is closed (if not present then use system default)"))
    }
}
